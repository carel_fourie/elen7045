Dodge game: ELEN7045 - Assignment 1.
==============================================================================================
Author: Carel Fourie

Level: Beginner  

Technologies: Java

Source: https://bitbucket.org/carel_fourie/elen7045/src 

What is it?
-----------
Dodge is a 2D computer game built with Java technology. 2D refers to two dimensions namely the X and Y dimensions.
Java is an operating system independent, run-time platform with an extensive software development kit.

The main Java packages used are:

java.awt.*,
java.swing.*,
java.util.*
(A * denotes multiple classes.)


Build it
-----------
Maven based project

JAVA Dynamix Proxy : ELEN7045 - Assignment 2.
==============================================================================================
Author: Carel Fourie

Level: Beginner  

Technologies: Java

Source: https://bitbucket.org/carel_fourie/elen7045/src 

What is it?
-----------
Example demonstrating the use of a JAVA Dynamic Proxy.

Scenario: A Client invokes a method on a Real Subject to which it passes a user id.
The Real Subject returns this information to the Client.
Unknown to the Client, a Proxy intercepts this call, does a security check against the user id,
and logs the response from the Real Subject before passing the response back to the Client.

Build it
-----------
Maven based project





