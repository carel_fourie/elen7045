package za.ac.wits.eie.elen7045.assignment2.dynamicproxy.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import za.ac.wits.eie.elen7045.assignment2.dynamicproxy.level.AccessLevel;

public class SecureInformationHandler implements InvocationHandler {

    private final Object realSubject;

    public SecureInformationHandler(Object realSubject) {
        this.realSubject = realSubject;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            int userId = (int) args[0];
            int accessLevel = getUserAccessLevel(userId);

            Object result = null;

            switch (accessLevel) {

                case AccessLevel.LEVEL_10:
                    System.out.println("proxy->real subject: Proxy cleared User Id: [" + userId + "] with Access Level: [10], proceed...");
                    result = method.invoke(realSubject, args);
                    return logResult(result, AccessLevel.LEVEL_10);

                case AccessLevel.LEVEL_20:
                    System.out.println("proxy->real subject: Proxy cleared User Id: [" + userId + "] with Access Level: [20], proceed...");
                    result = method.invoke(realSubject, args);
                    return logResult(result, AccessLevel.LEVEL_20);

                case AccessLevel.LEVEL_30:
                    System.out.println("proxy->real subject: Proxy cleared User Id: [" + userId + "] with Access Level: [30], proceed...");
                    result = method.invoke(realSubject, args);
                    return logResult(result, AccessLevel.LEVEL_30);
            }
            return "Unknown Access Level";
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        } catch (Exception e) {
            throw new RuntimeException("Unexpected invocation exception: " + e.getMessage());
        }
    }

    private Object logResult(Object result, int accessLevel) {
        System.out.println("proxy<-real subject: encrypted result with Access Level: [" + accessLevel + "], forward to client...");
        log(result);
        return result;
    }

    private int getUserAccessLevel(int userId) {
        if (userId < AccessLevel.LEVEL_10) {
            return AccessLevel.LEVEL_10;
        }
        if (userId < AccessLevel.LEVEL_20) {
            return AccessLevel.LEVEL_20;
        }
        if (userId < AccessLevel.LEVEL_30) {
            return AccessLevel.LEVEL_30;
        }
        return 0;
    }
    
    private void log(Object result) {
        // log to db?
    }
}
