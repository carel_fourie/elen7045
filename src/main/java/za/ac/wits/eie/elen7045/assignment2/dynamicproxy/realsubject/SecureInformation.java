package za.ac.wits.eie.elen7045.assignment2.dynamicproxy.realsubject;

import za.ac.wits.eie.elen7045.assignment2.dynamicproxy.subject.SecureInfoIFace;

public class SecureInformation implements SecureInfoIFace {

    @Override
    public String presentInfoToUser(int userId) {
        System.out.println("realsubject.presentInfoToUser(" + userId + ") invoked...");
        return "present information to User Id: [" + userId + "]";
    }
}
