package za.ac.wits.eie.elen7045.assignment2.dynamicproxy.level;

public class AccessLevel {

    public static final int LEVEL_10 = 10;

    public static final int LEVEL_20 = 20;

    public static final int LEVEL_30 = 30;

}
