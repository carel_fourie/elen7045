package za.ac.wits.eie.elen7045.assignment2.dynamicproxy.subject;

public interface SecureInfoIFace {

    String presentInfoToUser(int userId);

}
