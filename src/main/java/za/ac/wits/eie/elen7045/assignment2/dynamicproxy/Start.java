package za.ac.wits.eie.elen7045.assignment2.dynamicproxy;

import za.ac.wits.eie.elen7045.assignment2.dynamicproxy.factory.ProxyFactory;
import za.ac.wits.eie.elen7045.assignment2.dynamicproxy.realsubject.SecureInformation;
import za.ac.wits.eie.elen7045.assignment2.dynamicproxy.subject.SecureInfoIFace;

public class Start {

    public static void main(String[] args) {
        try {
            System.out.println("client->proxyfactory");
            SecureInfoIFace info =
                    (SecureInfoIFace) ProxyFactory.newInstance(new SecureInformation());
            System.out.println("client<-proxy<-proxyfactory");
            System.out.println("client->proxy.presentInfoToUser(15)");
            String result = info.presentInfoToUser(15);
            System.out.println("client<-proxy: " + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
