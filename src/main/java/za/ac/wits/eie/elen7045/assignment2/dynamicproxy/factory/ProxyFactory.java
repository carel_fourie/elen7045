package za.ac.wits.eie.elen7045.assignment2.dynamicproxy.factory;

import java.lang.reflect.Proxy;

import za.ac.wits.eie.elen7045.assignment2.dynamicproxy.handler.SecureInformationHandler;

public class ProxyFactory {

    public static Object newInstance(final Object obj) {
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(),
                                      obj.getClass().getInterfaces(),
                                      new SecureInformationHandler(obj));
    }
}
