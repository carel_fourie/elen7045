package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.base;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.cache.Cache;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.event.ModelEvent;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.event.ModelEventIFace;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.sprite.Sprite;

public abstract class BaseModel {

    public static void bruteForceCollision(List<BaseModel> models) {
        for (int p = 0; p < models.size(); p++) {
            for (int s = p + 1; s < models.size(); s++) {
                BaseModel model1 = models.get(p);
                BaseModel model2 = models.get(s);
                if (model1.collidesWith(model2)) {
                    model1.onCollision(model2);
                    model2.onCollision(model1);
                }
            }
        }
    }

    /** Horizontal speed (pixels/sec) */
    protected double dx;

    /** Vertical speed (pixels/sec) */
    protected double dy;

    protected List<ModelEventIFace> listeners = new ArrayList<ModelEventIFace>();

    /** Image representing this model */
    protected Sprite sprite;

    /** X location */
    protected double x;

    /** Y location */
    protected double y;

    /**
     * The rectangle representing the other model during brute force collision detection
     */
    private Rectangle otherModel = new Rectangle();

    /**
     * The rectangle representing this model during brute force collision detection
     */
    private Rectangle thisModel = new Rectangle();

    /**
     * Construct a model at XY location, represented by the image given.
     * 
     * @param x The initial X-axis location of this model
     * @param y The initial Y-axis location of this model
     * 
     * @param imagePath The path to the image representing this model
     */
    public BaseModel(int x, int y, String imagePath) {
        try {
            this.x = x;
            this.y = y;
            this.sprite = Cache.Sprites.getInstance().get(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BaseModel(ModelEventIFace listener, int x, int y, String imagePath) {
        this(x, y, imagePath);
        listeners.add(listener);
    }

    /**
     * Check if this model collides with another.
     * 
     * @param other The other model to check collision against
     * @return True if the entities collide with each other
     */
    public boolean collidesWith(BaseModel other) {
        thisModel.setBounds((int) x, (int) y, sprite.getWidth(), sprite.getHeight());
        otherModel.setBounds((int) other.x, (int) other.y, other.sprite.getWidth(), other.sprite.getHeight());
        return thisModel.intersects(otherModel);
    }

    /**
     * Draw this model to the graphics context provided
     * 
     * @param g The graphics context on which to draw
     */
    public void draw(Graphics g) {
        sprite.draw(g, (int) x, (int) y);
    }

    /**
     * Get the horizontal speed of this model
     * 
     * @return The horizontal speed of this model (pixels/sec)
     */
    public double getHorizontalMovement() {
        return dx;
    }

    /**
     * Get the vertical speed of this model
     * 
     * @return The vertical speed of this model (pixels/sec)
     */
    public double getVerticalMovement() {
        return dy;
    }

    /**
     * Get the x location of this model
     * 
     * @return The x location of this model
     */
    public int getX() {
        return (int) x;
    }

    /**
     * Get the y location of this model
     * 
     * @return The y location of this model
     */
    public int getY() {
        return (int) y;
    }

    /**
     * Request that this model move itself based on a certain ammount of time passing.
     * 
     * @param delta The ammount of time that has passed in milliseconds
     */
    public void move(long delta) {
        // update the location of the model based on move speeds
        x += (delta * dx) / 1000;
        y += (delta * dy) / 1000;
    }

    /**
     * Notification event of a collision.
     * 
     * @param other The model with which this model collided.
     */
    public abstract void onCollision(BaseModel other);

    /**
     * Allow derived classes to implement their own display or game logic
     */
    public abstract void performLogic();

    /**
     * Set horizontal speed of this model
     * 
     * @param dx Horizontal speed of this model (pixels/sec)
     */
    public void setHorizontalMovement(double dx) {
        this.dx = dx;
    }

    /**
     * Set vertical speed of this model
     * 
     * @param dy Vertical speed of this model (pixels/sec)
     */
    public void setVerticalMovement(double dy) {
        this.dy = dy;
    }

    protected synchronized void fireEvent(ModelEvent e) {
        for (ModelEventIFace listener : listeners) {
            listener.onModelEvent(e);
        }
    }
}
