package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.keyboard;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.event.GameInputEvent;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.event.GameInputEventFactory;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.event.GameInputIFace;

/**
 * Handle keyboard input. Fire relevant key press events.
 * 
 * @author Carel Fourie
 */
public class KeyboardInputManager extends KeyAdapter {

    private List<GameInputIFace> listeners = new ArrayList<GameInputIFace>();

    public KeyboardInputManager(GameInputIFace listener) {
        listeners.add(listener);
    }

    public synchronized void addEventListener(GameInputIFace listener) {
        listeners.add(listener);
    }

    /**
	 * 
	 */
    @Override
    public void keyPressed(KeyEvent e) {
        // super.keyPressed(e);
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            fireEvent(GameInputEventFactory.getLeftEvent());
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            fireEvent(GameInputEventFactory.getRighEvent());
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            fireEvent(GameInputEventFactory.getSpaceEvent());
        } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            fireEvent(GameInputEventFactory.getEscapeEvent());
        }
    }

    public synchronized void removeEventListener(GameInputIFace listener) {
        listeners.remove(listener);
    }

    private synchronized void fireEvent(GameInputEvent event) {
        for (GameInputIFace listener : listeners) {
            // System.out.println("fire");
            listener.onInputEvent(event);
        }
    }
}
