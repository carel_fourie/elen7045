package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.constant;

import java.util.Random;

public class DC {

    public static final String PLAYER_IMG = "player.jpg";

    public static final int PLAYER_SPEED = 300;

    public static final int RAIN_DROP_COUNT = 5;

    public static final String RAIN_IMG = "rain_drop.jpg";

    public static final int RAIN_SPEED = 200;

    public static final int SCREEN_X = 800;

    public static final int SCREEN_Y = 600;

    public static int[] getRainStartPos() {
        int[] xy = new int[2];

        // x = 0 to DC.SCREEN_X
        int i = DC.SCREEN_X - new Random().nextInt(DC.SCREEN_X + 1);
        xy[0] = i;

        // y = 0 to 150
        i = (DC.SCREEN_Y / 4) - new Random().nextInt((DC.SCREEN_Y / 4));
        xy[1] = i;

        return xy;
    }
}
