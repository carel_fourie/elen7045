package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.event;

/**
 * Simple factory class for convenience.
 * 
 * @author Carel Fourie
 */
public class GameInputEventFactory {

    public static GameInputEvent getEscapeEvent() {
        GameInputEvent e = new GameInputEvent(new GameInputEventFactory());
        e.setEscape(true);
        return e;
    }

    public static GameInputEvent getLeftEvent() {
        GameInputEvent e = new GameInputEvent(new GameInputEventFactory());
        e.setLeft(true);
        return e;
    }

    public static GameInputEvent getRighEvent() {
        GameInputEvent e = new GameInputEvent(new GameInputEventFactory());
        e.setRight(true);
        return e;
    }

    public static GameInputEvent getSpaceEvent() {
        GameInputEvent e = new GameInputEvent(new GameInputEventFactory());
        e.setSpace(true);
        return e;
    }
}
