package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.event;

public interface ModelEventIFace {

    void onModelEvent(ModelEvent e);

}
