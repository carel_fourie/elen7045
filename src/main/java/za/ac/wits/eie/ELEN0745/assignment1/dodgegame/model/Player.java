package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model;

import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.constant.DC;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.base.BaseModel;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.event.ModelEvent;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.event.ModelEventIFace;

public class Player extends BaseModel {

    public Player(int x, int y, String imagePath) {
        super(x, y, imagePath);
    }

    public Player(ModelEventIFace listener, int x, int y, String imagePath) {
        super(listener, x, y, imagePath);
    }

    @Override
    public void move(long delta) {
        // if we're moving left and have reached the left hand side
        // of the screen, don't move
        if ((dx < 0) && (x < (this.sprite.getWidth() - 100))) {
            return;
        }
        // if we're moving right and have reached the right hand side
        // of the screen, don't move
        if ((dx > 0) && (x > (DC.SCREEN_X - this.sprite.getWidth()))) {
            return;
        }
        super.move(delta);
    }

    @Override
    public void onCollision(BaseModel other) {
        ModelEvent e = new ModelEvent(this);
        e.setCollission(true);
        fireEvent(e);
    }

    @Override
    public void performLogic() {
    }
}
