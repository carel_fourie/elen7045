package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.sprite;

import java.awt.Graphics;
import java.awt.Image;

public class Sprite {

    private Image image;

    public Sprite(Image image) {
        this.image = image;
    }

    public void draw(Graphics g, int x, int y) {
        g.drawImage(image, x, y, null);
    }

    public int getHeight() {
        return image.getHeight(null);
    }

    public int getWidth() {
        return image.getWidth(null);
    }
}