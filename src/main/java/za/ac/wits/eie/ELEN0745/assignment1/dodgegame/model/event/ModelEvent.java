package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.event;

import java.util.EventObject;

public class ModelEvent extends EventObject {

    private static final long serialVersionUID = -989410344494235287L;

    private boolean collission = false;

    public ModelEvent(Object source) {
        super(source);
    }

    public boolean isCollission() {
        return collission;
    }

    public void setCollission(boolean collission) {
        this.collission = collission;
    }
}
