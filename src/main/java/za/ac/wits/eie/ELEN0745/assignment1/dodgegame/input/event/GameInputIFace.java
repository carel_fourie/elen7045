package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.event;

public interface GameInputIFace {

    void onInputEvent(GameInputEvent e);

}
