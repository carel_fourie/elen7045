package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.event;

import java.util.EventObject;

public class GameInputEvent extends EventObject {

    private static final long serialVersionUID = -8453964296191383598L;

    private Boolean down = false;

    private Boolean escape = false;

    private Boolean left = false;

    private Boolean right = false;

    private Boolean space = false;

    private Boolean up = false;

    public GameInputEvent(Object source) {
        super(source);
    }

    public Boolean isDown() {
        return down;
    }

    public Boolean isEscape() {
        return escape;
    }

    public Boolean isLeft() {
        return left;
    }

    public Boolean isRight() {
        return right;
    }

    public Boolean isSpace() {
        return space;
    }

    public Boolean isUp() {
        return up;
    }

    public void reset() {
        down = false;
        escape = false;
        left = false;
        right = false;
        space = false;
        up = false;
    }

    public void setDown(Boolean down) {
        this.down = down;
    }

    public void setEscape(Boolean escape) {
        this.escape = escape;
    }

    public void setLeft(Boolean left) {
        this.left = left;
    }

    public void setRight(Boolean right) {
        this.right = right;
    }

    public void setSpace(Boolean space) {
        this.space = space;
    }

    public void setUp(Boolean up) {
        this.up = up;
    }
}
