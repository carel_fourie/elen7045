package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model;

import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.constant.DC;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.base.BaseModel;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.event.ModelEventIFace;

public class RainDrop extends BaseModel {

    public RainDrop(int x, int y, String imagePath) {
        super(x, y, imagePath);
    }

    public RainDrop(ModelEventIFace listener, int x, int y, String imagePath) {
        super(listener, x, y, imagePath);
    }

    @Override
    public void move(long delta) {
        if ((dy > 0) && (y > DC.SCREEN_Y)) {
            return;
        }
        super.move(delta);
    }

    @Override
    public void onCollision(BaseModel other) {
    }

    @Override
    public void performLogic() {
        if (y > DC.SCREEN_Y) {
            int[] xy = DC.getRainStartPos();
            x = xy[0];
            y = xy[1];
        }
    }
}
