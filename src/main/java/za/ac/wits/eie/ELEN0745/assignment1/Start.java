package za.ac.wits.eie.ELEN0745.assignment1;

import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.Dodge;

/**
 * @author Carel Fourie
 * 
 */
public class Start {

    /***
     * Start...
     * <p>
     * Simply run this class as a Java application from your IDE.
     * 
     */
    public static void main(String[] args) {
        new Dodge().run();
    }
}
