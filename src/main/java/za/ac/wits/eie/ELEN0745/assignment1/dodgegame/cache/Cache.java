package za.ac.wits.eie.ELEN0745.assignment1.dodgegame.cache;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.imageio.ImageIO;

import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.sprite.Sprite;

public class Cache {

    public static class Sprites {

        private static class SingletonHolder {
            public static final Sprites INSTANCE = new Sprites();
        }

        public static Sprites getInstance() {
            return SingletonHolder.INSTANCE;
        }

        private final ConcurrentMap<String, Sprite> sprites;

        private Sprites() {
            sprites = new ConcurrentHashMap<String, Sprite>();
        }

        public void add(String key, Sprite sprite) {
            if (sprites.containsKey(key)) {
                sprites.remove(key);
            }
            sprites.put(key, sprite);
        }

        public Sprite get(String key) throws IOException {
            Sprite sprite = sprites.get(key);
            if (sprite == null) {
                return new Cache().getSprite(key);
            }
            return sprite;
        }
    }

    private Sprite getSprite(String spriteName) throws IOException {
        try {
            // runnable
            // URL url =
            // this.getClass().getClassLoader().getResource("resources/" +
            // spriteName);

            // ide
            URL url = this.getClass().getClassLoader().getResource(spriteName);

            BufferedImage bufferedImage = ImageIO.read(url);

            GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
            Image image = gc.createCompatibleImage(bufferedImage.getWidth(), bufferedImage.getHeight(), Transparency.OPAQUE);

            image.getGraphics().drawImage(bufferedImage, 0, 0, null);

            Sprite sprite = new Sprite(image);
            Cache.Sprites.getInstance().add(spriteName, sprite);

            return sprite;
        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }
}
