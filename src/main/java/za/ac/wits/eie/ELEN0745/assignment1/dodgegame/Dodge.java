package za.ac.wits.eie.ELEN0745.assignment1.dodgegame;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.constant.DC;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.event.GameInputEvent;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.event.GameInputIFace;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.input.keyboard.KeyboardInputManager;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.Player;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.RainDrop;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.base.BaseModel;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.event.ModelEvent;
import za.ac.wits.eie.ELEN0745.assignment1.dodgegame.model.event.ModelEventIFace;

/**
 * 
 * The main run loop is contained in this class.
 * 
 * 
 * @author Carel Fourie
 * 
 */
public class Dodge extends Canvas implements GameInputIFace, ModelEventIFace {

    private static final long serialVersionUID = -3927674076986859259L;

    private BufferStrategy bufferStrategy;

    private GameInputEvent gameInputEvent;

    private Boolean inPlay = true;

    private List<BaseModel> models = new ArrayList<BaseModel>();

    private Player player1;

    public Dodge() {
        // init our event object, null pointers aren't good
        gameInputEvent = new GameInputEvent(this);

        // create main frame
        JFrame container = new JFrame("ELEN0745 - Dodge Game - Carel Fourie");

        JPanel panel = (JPanel) container.getContentPane();
        panel.setPreferredSize(new Dimension(DC.SCREEN_X, DC.SCREEN_Y));
        panel.setLayout(null);
        setBounds(0, 0, DC.SCREEN_X, DC.SCREEN_Y);
        panel.add(this);

        setIgnoreRepaint(true);

        // show
        container.pack();
        container.setResizable(false);
        container.setVisible(true);

        // exit game on close
        container.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        // set keyboard input
        KeyboardInputManager kb = new KeyboardInputManager(this);
        addKeyListener(kb);

        // to manage our accelerated graphics
        createBufferStrategy(2);
        bufferStrategy = getBufferStrategy();

        requestFocus();

        createModels();
    }

    @Override
    public void onInputEvent(GameInputEvent e) {
        // System.err.println("fire rec");
        gameInputEvent = e;
    }

    @Override
    public void onModelEvent(ModelEvent e) {
        if (e.isCollission()) {
            inPlay = false;
            JOptionPane.showMessageDialog(this, "You loose!");
        }
    }

    public void run() {
        long start = System.currentTimeMillis();
        while (inPlay) {
            long delta = System.currentTimeMillis() - start;
            start = System.currentTimeMillis();
            Graphics2D g2d = (Graphics2D) bufferStrategy.getDrawGraphics();
            g2d.setColor(Color.WHITE);
            g2d.fillRect(0, 0, DC.SCREEN_X, DC.SCREEN_Y);
            processModels(g2d, delta);
            g2d.dispose();
            bufferStrategy.show();
            processInputEvents();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void createModels() {
        // player
        player1 = new Player(this, (DC.SCREEN_X / 2), 470, DC.PLAYER_IMG);
        models.add(player1);

        // rain
        for (int i = 0; i < DC.RAIN_DROP_COUNT; i++) {
            int[] xy = DC.getRainStartPos();
            RainDrop rd = new RainDrop(this, xy[0], xy[1], DC.RAIN_IMG);
            models.add(rd);
        }
    }

    private void movePlayer(double moveSpeed) {
        player1.setHorizontalMovement(moveSpeed);
    }

    private void processInputEvents() {
        if ((gameInputEvent.isLeft())) {
            movePlayer(-DC.PLAYER_SPEED);
        } else if (gameInputEvent.isRight()) {
            movePlayer(DC.PLAYER_SPEED);
        } else if (gameInputEvent.isSpace()) {
            System.out.println("space");
        } else if (gameInputEvent.isEscape()) {
            inPlay = false;
        }
    }

    private void processModels(Graphics2D g2d, long delta) {
        // move
        for (BaseModel model : models) {
            model.move(delta);
        }
        // draw
        for (BaseModel model : models) {
            model.draw(g2d);
        }
        // collision?
        BaseModel.bruteForceCollision(models);
        // rain, and rain reset logic
        for (BaseModel model : models) {
            if (model instanceof RainDrop) {
                model.setVerticalMovement(DC.RAIN_SPEED);
                model.performLogic();
            }
        }
    }
}
